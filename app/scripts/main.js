'use strict';



$( document ).ready(function() {
  	
  var $body = $('body');
	var $callsWarper = $('.modulo-all-calls-warper');

	$('.open-sms-notes,.sms-cancel').on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		$this.parents('.sms,.notes,.make-action').toggleClass('opened');
	});


	$('#options-call-filter input').on('change', function() {
   		console.log('aqui');
   		var opt = $('input[name=call-filter]:checked', '#options-call-filter').val();
   		var hideOrNot = $('input[id=hide-choosen]:checked', '#options-call-filter').val();

   		$callsWarper.find('.call-sumary').each(function(i,e){
   			var $call = $(e);
   			$call.removeClass('dimmed')
   				 .removeClass('hidden');

   			if($call.hasClass(opt)) {
   				$call.addClass('dimmed');
   			}

   			if($call.hasClass('dimmed') && hideOrNot === 'hide' ) {
   				$call.addClass('hidden');
   			}
   		});

	});


	$('.transition-to-resume-by-caller').on('click', function(e){
		e.preventDefault();
		$body.addClass('show-resume-callid');
	});

  $('.transition-to-calls-activity').on('click', function(e){
    e.preventDefault();
    $body.removeClass('show-resume-callid');
  });

});

var client = require('twilio')('AC37d9214c2de4b16ab1dd6fd788b7c5bb', '7985f6ebfbf5e052db1e49c2e3e36d04');

//Send an SMS text message
client.sendMessage({

    to:'+351932555526', // Any number Twilio can deliver to
    from: '+13234982954', // A number you bought from Twilio and can use for outbound communication
    body: 'word to your mother.' // body of the SMS message

}, function(err, responseData) { //this function is executed when a response is received from Twilio

    if (!err) { // "err" is an error received during the request, if any

        // "responseData" is a JavaScript object containing data received from Twilio.
        // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
        // http://www.twilio.com/docs/api/rest/sending-sms#example-1

        console.log(responseData.from); // outputs "+14506667788"
        console.log(responseData.body); // outputs "word to your mother."

    }
});

