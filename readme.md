# TALKDEST - STUDIE


### Installation - Dependencies

You need Node JS [node js](https://nodejs.org/)

You need grunt installed globally:

```sh
$ npm install -g grunt-cli
```
and Bower installed. Bower is a command line utility.
```sh
$ npm install -g bower
```

### Installation

```sh
$ git clone git@bitbucket.org:luispteixeira/td_app.git
$ cd td_app
$ npm install
$ bower install
$ grunt serve
```

### Development & Launch Local Server

watch js, less and do livereload:
```sh
$ grunt serve
```
### Distribution
distrubution task (compress scss, minify css, autoprefix, uglify, ect)
```sh
$ grunt build
```
